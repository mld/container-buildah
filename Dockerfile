FROM ubuntu:20.04

# Make sure the locale isn't acting up
ENV LANG C.UTF-8

# Fixes some weird terminal issues such as broken clear / CTRL+L
ENV TERM=linux

# Ensure apt doesn't ask questions when installing stuff
ENV DEBIAN_FRONTEND=noninteractive

RUN . /etc/os-release \
	&& apt-get update -qq \
	&& apt-get -qq -y install wget gnupg curl \
	&& echo "deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/x${NAME}_${VERSION_ID}/ /" > /etc/apt/sources.list.d/devel_kubic_libcontainers_stable.list \
	&& wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/x${NAME}_${VERSION_ID}/Release.key -O Release.key \
	&& apt-key add - < Release.key \
	&& apt-get update -qq \
	&& apt-get -qq -y install buildah \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*
